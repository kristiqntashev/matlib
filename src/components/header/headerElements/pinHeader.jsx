import { useState } from "react";
export const [headerPin, setHeaderPin] = useState(false);
export function togglePin() {
  setHeaderPin(!headerPin);
}
