import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

export const Profile = (props) => {
  const { user } = props;
  return (
    <Link to="profile">
      <div className="user">
        <span>Hello, {user.firstName ? user.firstName : user.email}!</span>
        <div
          className="profile-pic"
          style={
            user.photoUrl
              ? {
                  backgroundImage: `url(${user.photoUrl})`
                }
              : {
                  backgroundImage: `url(https://www.jbrhomes.com/wp-content/uploads/blank-avatar.png)`
                }
          }
        ></div>
      </div>
    </Link>
  );
};

Profile.propTypes = {
  user: PropTypes.object
};
