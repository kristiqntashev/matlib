import React from "react";
import { Link } from "react-router-dom";
import FavoritesIcon from "../../../services/header/favorites/FavoritesIcon";
import FavoritesNumber from "../../../containers/favorites/FavoritesNumber/FavoritesNumber";
import { values } from "../../../constants";
import PropTypes from "prop-types";

export const Favorites = (props) => {
  return props.favoritesList.length > 0 ? (
    <Link
      to={values.Links.favoritesPage.link}
      className={values.Links.favoritesPage.className}
    >
      <FavoritesIcon />
      <FavoritesNumber />
    </Link>
  ) : (
    <div className={values.Links.favoritesPage.className}>
      <FavoritesIcon />
      <FavoritesNumber />
    </div>
  );
};

Favorites.propTypes = {
  favoritesList: PropTypes.array
};
