import React from "react";
import "./Tooltip.css";
import PropTypes from "prop-types";

const Tooltip = (props) => {
  return <span className={props.className}>{props.content}</span>;
};

Tooltip.propTypes = {
  className: PropTypes.string
};

Tooltip.propTypes = {
  content: PropTypes.element
};

export default Tooltip;
