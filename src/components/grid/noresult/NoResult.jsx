import React from "react";
import propTypes from "prop-types";

function NoResult(props) {
  return (
    <>
      <div className="no-result" key="no-result">
        <div>
          <p>Ups.. sorry,</p> {props.message}
        </div>
      </div>
    </>
  );
}
NoResult.propTypes = {
  message: propTypes.string
};

export default NoResult;
