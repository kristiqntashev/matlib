export const handleScroll = (setUrlParams) => {
  const scrollTop =
    (document.documentElement && document.documentElement.scrollTop) ||
    document.body.scrollTop;
  const scrollHeight =
    (document.documentElement && document.documentElement.scrollHeight) ||
    document.body.scrollHeight;
  if (scrollTop != 0 && scrollTop + window.innerHeight + 50 >= scrollHeight) {
    setUrlParams((pageNumber) => pageNumber + 1);
  }
};
