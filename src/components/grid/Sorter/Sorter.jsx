import React from "react";
import propTypes from "prop-types";

const Sorter = (props) => {
  const { sort } = props;

  function changeSorting(e) {
    const newSorterType = e.target.value;
    sort(newSorterType);
  }
  return (
    <div className="sorter">
      <span className="sorter--label">Sort by: </span>
      <select id="sorter" onChange={changeSorting}>
        <option value="">recommended</option>
        <option value="name">name</option>
        <option value="createdAt">date</option>
      </select>
    </div>
  );
};

Sorter.propTypes = {
  sort: propTypes.func
};

export default Sorter;
