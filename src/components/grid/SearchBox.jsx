import React, { useState } from "react";
import propTypes from "prop-types";

const SearchBox = (props) => {
  const { search } = props;
  const [value, setValue] = useState("");
  console.log("box");
  console.log(props);

  function setSearch(e) {
    setValue(e.target.value);
  }
  function changeSearch() {
    search(value);
  }
  return (
    <div className="search-holder">
      <i className="fas fa-search"></i>
      <input type="search" className="search" onChange={setSearch} />
      <button className="search__button" onClick={changeSearch}>
        Search
      </button>
    </div>
  );
};

SearchBox.propTypes = {
  search: propTypes.func
};

export default SearchBox;
