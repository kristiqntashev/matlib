import FilterPill from "../FilterPill/FilterPill";

import React from "react";
import propTypes from "prop-types";

function Pills(props) {
  return props.currentFilters.map((filter) => (
    <FilterPill {...props} filter={filter} key={filter} />
  ));
}

Pills.propTypes = {
  currentFilters: propTypes.array.isRequired
};

export default Pills;
