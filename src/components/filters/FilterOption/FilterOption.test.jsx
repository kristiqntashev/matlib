import React from "react";
import { shallow } from "enzyme";
import FilterOption from "./FilterOption";

const option = { name: "Leather", id: 2 };
const option2 = { name: "Copper", id: 5, hex: "#333333" };

describe("Test component: <FilterOption />", () => {
  it("Renders pill without crashing: <FilterOption />", () => {
    const wrapper = shallow(
      <FilterOption
        filterType={"materials"}
        handleChange={() => {}}
        currentFilters={["materials_2_Leather"]}
        isColor={true}
        option={option}
        key={option.id + option.name}
      />
    );

    expect(wrapper.exists()).toBe(true);
  });

  it("Should render 1 div in FilterOption", () => {
    const wrapper = shallow(
      <FilterOption
        filterType={"materials"}
        handleChange={() => {}}
        currentFilters={["materials_2_Leather"]}
        isColor={false}
        option={option}
        key={option.id + option.name}
      />
    );
    expect(wrapper.find("div").length).toBe(1);
  });

  it("Should render 1 label in FilterOption", () => {
    const wrapper = shallow(
      <FilterOption
        filterType={"materials"}
        handleChange={() => {}}
        currentFilters={[]}
        isColor={false}
        option={option}
        key={option.id + option.name}
      />
    );
    expect(wrapper.find("label").length).toBe(1);
  });

  it("Should render 1 input in FilterOption", () => {
    const wrapper = shallow(
      <FilterOption
        filterType={"materials"}
        handleChange={() => {}}
        currentFilters={[]}
        isColor={false}
        option={option}
        key={option.id + option.name}
      />
    );
    expect(wrapper.find("input").length).toBe(1);
  });

  it("Should render 1 li with class swatch in FilterOption", () => {
    const wrapper = shallow(
      <FilterOption
        filterType={"colors"}
        handleChange={() => {}}
        currentFilters={[]}
        isColor={true}
        option={option2}
        key={option2.id + option2.name}
      />
    );
    expect(wrapper.find(".filter-option--color-swatch").length).toBe(1);
  });

  it("Should render 1 div with style FilterOption", () => {
    const wrapper = shallow(
      <FilterOption
        filterType={"colors"}
        handleChange={() => {}}
        currentFilters={[]}
        isColor={true}
        option={option2}
        key={option2.id + option2.name}
      />
    );

    const styledDiv = wrapper.find(".filter-option--custom-check");

    expect(styledDiv.props()).toHaveProperty("style.backgroundColor", option2.hex);
    expect(styledDiv.props()).toHaveProperty(
      "style.background",
      `linear-gradient(45deg,  ${option2.hex} 30%,rgba(255,255,255,1) 50%,${option2.hex} 80%)`
    );
  });
});
