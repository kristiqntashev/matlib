import React from "react";
import { shallow } from "enzyme";
import FilterPill from "./FilterPill";

describe("Test component: <FilterPill />", () => {
  it("Renders pill without crashing: <FilterPill />", () => {
    const wrapper = shallow(
      <FilterPill filter={"materials_2_Leather"} handleChange={() => {}} />
    );
    expect(wrapper.exists()).toBe(true);
  });

  it("Should render 1 div in FilterPill", () => {
    const wrapper = shallow(
      <FilterPill filter={"materials_2_Leather"} handleChange={() => {}} />
    );
    expect(wrapper.find("div").length).toBe(1);
  });

  it("Should render 1 label in FilterPill", () => {
    const wrapper = shallow(
      <FilterPill filter={"materials_2_Leather"} handleChange={() => {}} />
    );
    expect(wrapper.find("label").length).toBe(1);
  });

  it("Should render 1 input in FilterPill", () => {
    const wrapper = shallow(
      <FilterPill filter={"materials_2_Leather"} handleChange={() => {}} />
    );
    expect(wrapper.find("input").length).toBe(1);
  });

  it("Should render tag: prefix in FilterPill", () => {
    const wrapper = shallow(
      <FilterPill filter={"tags_2_Matte"} handleChange={() => {}} />
    );
    expect(wrapper.find("label").text().indexOf("tag: ")).toBe(0);
  });
});
