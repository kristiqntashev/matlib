import React, { useState } from "react";
import "./login-screen.css";
import propTypes from "prop-types";
import config from "../../../services/config.json";
import { Redirect } from "react-router-dom";
import { decodeToken } from "react-jwt";

function LoginScreen(props) {
  const { logIn, user, fetchFavorites } = props;
  const [credentials, setCredentials] = useState({ email: "", password: "" });
  const [serverErrorResponse, setServerErrorResponse] = useState(null);

  function setEmail(event) {
    const result = event.target;
    setCredentials((credentials) => ({
      ...credentials,
      email: result.value
    }));
  }

  function setPassword(event) {
    const result = event.target;
    setCredentials((credentials) => ({
      ...credentials,
      password: result.value
    }));
  }

  function handleSubmit(event) {
    event.preventDefault();
    const baseUrl = config["BASE_URL"];
    const path = "login";
    const requestUrl = `${baseUrl}${path}`;
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ ...credentials })
    };
    fetch(requestUrl, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        data.accessToken ? getUser(data.accessToken) : setServerErrorResponse(data);
      });
    fetchFavorites();
  }

  const getUser = (accessToken) => {
    const decode = decodeToken(accessToken);
    localStorage.setItem("accessToken", accessToken);
    const requestUrl = `https://matlib.herokuapp.com/users?id=${decode.sub}`;
    fetch(requestUrl)
      .then((response) => response.json())
      .then((data) => {
        logIn(data[0]);
      });
  };

  const loginForm = () => (
    <div className="login">
      <h1>MatLib</h1>
      <div className="login-container">
        <h2>Sign In</h2>
        <form onSubmit={(event) => handleSubmit(event)}>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            onChange={(event) => setEmail(event)}
          ></input>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            onChange={(event) => setPassword(event)}
          ></input>
          {serverErrorResponse ? <>{serverErrorResponse}</> : <></>}
          <input
            type="submit"
            className="login-container__button"
            value="Sign In"
          ></input>
        </form>
        <div className="login-container__message">
          Trouble signing in? Contact your account manager.
        </div>
      </div>
    </div>
  );

  return user ? <Redirect to="/" /> : loginForm();
}

LoginScreen.propTypes = {
  credentials: propTypes.object,
  logIn: propTypes.func.isRequired,
  user: propTypes.object,
  fetchFavorites: propTypes.func
};

export default LoginScreen;
