import React from "react";
import { shallow } from "enzyme";
import UserDetails from "./UserDetails";
import { default as data } from "../../../../data";

describe("Test component: <UserDetails />", () => {
  it("Renders tags without crashing: <UserDetails />", () => {
    const userDetails = shallow(
      <UserDetails user={data.users[0]} handleChange={() => {}} />
    );

    expect(userDetails.exists()).toBe(true);
  });
});
