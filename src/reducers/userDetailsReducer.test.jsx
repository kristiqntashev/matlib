import { userDetailsReducer } from "./userDetailsReducer";
import { editDetails } from "../actions/userDetailsActions";

describe("edit user details", () => {
  it("edit details", () => {
    const next = userDetailsReducer(
      [],
      editDetails({ type: "EDIT_DETAILS", details: [{}] })
    );
    expect(next).toMatchSnapshot();
  });
  it("return default", () => {
    const next = userDetailsReducer(null, { type: "NO_TYPE" });
    expect(next).toMatchSnapshot();
  });
});