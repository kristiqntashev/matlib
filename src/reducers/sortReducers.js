import { SORT } from "../actions/constants";

export const sortReducers = (state = "", action) => {
  switch (action.type) {
    case SORT:
      return action.sorterType;
    default:
      return state;
  }
};
