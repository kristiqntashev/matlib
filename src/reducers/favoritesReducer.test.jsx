import { favoritesListReducer } from "./favoritesNumberReducer";
import { addFav, removeFav, getFavs } from "../actions/favoritesListActions";

describe("favorites reducers", () => {
  it("adds favorite", () => {
    const next = favoritesListReducer(
      [],
      addFav({ type: "ADD_FAV", productId: {} })
    );
    expect(next).toMatchSnapshot();
  });
  it("removes favorite", () => {
    const next = favoritesListReducer(
      [],
      removeFav({ type: "REMOVE_FAV", productId: {} })
    );
    expect(next).toMatchSnapshot();
  });
  it("gets favorites", () => {
    const next = favoritesListReducer(
      [],
      getFavs({ type: "FETCH_FAVORITES_SUCCESS", favorites: [{}] })
    );
    expect(next).toMatchSnapshot();
  });
  it("gets default", () => {
    const next = favoritesListReducer([], getFavs({ type: "NO_TYPE" }));
    expect(next).toMatchSnapshot();
  });
});
