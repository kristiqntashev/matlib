import { LOG_IN } from "../actions/constants";
import { LOG_OUT } from "../actions/constants";
import { UPDATE_USER } from "../actions/constants";

export const logInReducer = (state = null, action) => {
  switch (action.type) {
    case LOG_IN:
      return action.user;
    case LOG_OUT:
      return action.user;
    case UPDATE_USER:
      return action.user;
    default:
      return state;
  }
};
