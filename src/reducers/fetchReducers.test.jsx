import { productsFetchReducer } from "./fetchReducers";
import {
  fetchProductsSpinner,
  fetchProductsSuccess,
  clearProducts,
  fetchProductManufacturersAction
} from "../actions/fetchingActions";

import {
  SPINNER,
  FETCH_SUCCESS,
  FETCH_SUCCESS_MANUFACTURER,
  CLEAR_PRODUCTS
} from "../actions/constants";

const initialState = {
  spinner: false,
  products: [],
  productsNum: null,
  error: null
};

describe("fetch reducers", () => {
  it("returns spinner", () => {
    const next = productsFetchReducer(
      initialState,
      fetchProductsSpinner({ type: SPINNER })
    );
    expect(next).toMatchSnapshot();
  });
  it("clear products", () => {
    const next = productsFetchReducer(
      initialState,
      clearProducts({
        type: CLEAR_PRODUCTS,
        products: [
          { key: 1, id: 1 },
          { key: 2, id: 2 },
          { key: 3, id: 3 }
        ]
      })
    );
    expect(next).toMatchSnapshot();
  });
  it("returns manufacturers", () => {
    const next = productsFetchReducer(
      initialState,
      fetchProductManufacturersAction({
        type: FETCH_SUCCESS_MANUFACTURER,
        manufacturers: [
          { key: 1, id: 1 },
          { key: 2, id: 2 },
          { key: 3, id: 3 }
        ]
      })
    );
    expect(next).toMatchSnapshot();
  });
});
