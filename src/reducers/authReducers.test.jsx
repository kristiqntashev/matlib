import { logInReducer } from "./authReducers";
import { logIn, logOut } from "../actions/authActions";

describe("authorisation reducers", () => {
  it("logs in user", () => {
    const next = logInReducer(null, logIn({ type: "LOG_IN", user: [{}] }));
    expect(next).toMatchSnapshot();
  });
  it("logs out user", () => {
    const next = logInReducer(null, logOut({ type: "LOG_OUT", user: [{}] }));
    expect(next).toMatchSnapshot();
  });
  it("return default", () => {
    const next = logInReducer(null, logIn());
    expect(next).toMatchSnapshot();
  });
});
