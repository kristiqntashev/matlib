import { SEARCH } from "../actions/constants";

export const searchingReducer = (state = "", action) => {
  switch (action.type) {
    case SEARCH:
      return action.searching;
    default:
      return state;
  }
};
