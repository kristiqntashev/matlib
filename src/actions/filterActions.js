import { MAKE_LIST } from "./constants";

export const filter = (currentFilters) => ({
  type: MAKE_LIST,
  currentFilters
});
