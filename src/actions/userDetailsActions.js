import { EDIT_DETAILS } from "./constants";
import { UPDATE_USER } from "./constants";

export const editDetails = (details) => ({
  type: EDIT_DETAILS,
  details
});

export const userUpdateAction = (user) => ({
  type: UPDATE_USER,
  user
});
