import { useState, useEffect } from "react";
import config from "./config.json";

const baseUrl = config["BASE_URL"];

export default function useFetchLogin(loginData) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const path = "login";
  const requestUrl = `${baseUrl}${path}`;

  useEffect(() => {
    async function init() {
      try {
        let response;
        const requestOptions = {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ loginData })
        };
        response = await fetch(requestUrl, requestOptions);
        if (response.ok) {
          const json = await response.json();
          setData(json);
        } else {
          throw response;
        }
      } catch (e) {
        setError(e);
      } finally {
        setLoading(false);
      }
    }
    init();
  }, []);

  return { data, error, loading };
}
