import {
  fetchProductsSpinner,
  fetchProductsSuccess,
  fetchProductManufacturersAction
} from "../actions/fetchingActions";

import { getFavs, addFav, removeFav } from "../actions/favoritesListActions";
import { editDetails, userUpdateAction } from "../actions/userDetailsActions";
import { decodeToken } from "react-jwt";
import { values } from "../constants";

const token = localStorage.getItem("accessToken");
const decode = decodeToken(token);

const headers = () => {
  const myHeaders = new Headers();
  myHeaders.append("Authorization", `Bearer ${token}`);
  myHeaders.append("Content-Type", "application/json");
  return myHeaders;
};

const getRequestOptionsRaw = (method, raw = "") => {
  const requestOptions = {
    method: method,
    headers: headers(),
    body: raw,
    redirect: "follow"
  };
  return requestOptions;
};

const getRequestOptionsNoRaw = (method) => {
  const requestOptions = {
    method: method,
    headers: headers(),
    redirect: "follow"
  };
  return requestOptions;
};

export const fetchProducts = (
  params = { page: 1, filters: [], productIds: "", sort: "", searching: "" }
) => {
  return (dispatch) => {
    const { page, filters, productIds, sort, searching } = params;
    console.log("sort");
    console.log(searching);
    const filtersQuery = filters
      .map((f) => {
        const filterData = f.split("_");
        const filterType = filterData[0];
        const filterId = filterData[1];
        return "&" + values.FilterQueryKeys[filterType] + "=" + filterId;
      })
      .join("");
    fetch(
      `https://matlib.herokuapp.com/vrscans?_page=${page}&_limit=20&${filtersQuery}&${productIds}&_sort=${sort}&name_like=${searching}`
    )
      .then((result) => result.json())
      .then((result) => {
        dispatch(fetchProductsSuccess(result));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const fetchProductsWith = (params = { page: 1, filters: [] }) => {
  return (dispatch) => {
    const { page, filters } = params;
    const filtersQuery = filters
      .map((f) => {
        const filterData = f.split("_");
        const filterType = filterData[0];
        const filterId = filterData[1];
        return "&" + values.FilterQueryKeys[filterType] + "=" + filterId;
      })
      .join("");
    dispatch(fetchProductsSpinner());
    fetch(`https://matlib.herokuapp.com/vrscans?_page=${page}${filtersQuery}`)
      .then((result) => result.json())
      .then((result) => {
        dispatch(fetchProductsSuccess(result));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const fetchFavorites = () => {
  return (dispatch) => {
    fetch(
      `https://matlib.herokuapp.com/favorites?userId=${decode.sub}`,
      getRequestOptionsNoRaw("GET")
    )
      .then((response) => response.json())
      .then((result) => {
        dispatch(getFavs(result));
      })
      .catch((error) => console.log("error", error));
  };
};
export const fetchManufacturer = () => {
  return async (dispatch) => {
    await fetch(`https://matlib.herokuapp.com/manufacturers`)
      .then((result) => result.json())
      .then((result) => {
        dispatch(fetchProductManufacturersAction(result));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const editUserDetails = (details) => {
  return (dispatch) => {
    fetch(
      `https://matlib.herokuapp.com/users/${details.id}`,
      getRequestOptionsRaw("PATCH", JSON.stringify({ ...details }))
    )
      .then((response) => response.json())
      .then((result) => {
        dispatch(editDetails(result));
        dispatch(userUpdateAction(details));
      })
      .catch((error) => console.log("error", error));
  };
};
export const addFavorites = (productId) => {
  return (dispatch) => {
    fetch(
      `https://matlib.herokuapp.com/favorites`,
      getRequestOptionsRaw(
        "POST",
        JSON.stringify({ vrscanId: productId, userId: decode.sub })
      )
    )
      .then((response) => response.json())
      .then((result) => {
        dispatch(addFav(result));
      })
      .catch((error) => console.log("error", error));
  };
};

export const removeFavorites = (favoriteId) => {
  return (dispatch) => {
    fetch(
      `https://matlib.herokuapp.com/favorites/${favoriteId}`,
      getRequestOptionsNoRaw("DELETE")
    )
      .then(() => {
        dispatch(removeFav(favoriteId));
      })
      .catch((error) => console.log("error", error));
  };
};
