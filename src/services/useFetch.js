import { useState, useEffect } from "react";
import config from "./config.json";
import { values } from "../constants";

const baseUrl = config["BASE_URL"];

export default function useFetch(path, urlParams) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const getFiltersString = (urlParams) => {
    return urlParams.filters
      .map((f) => {
        const filterData = f.split("_");
        const filterType = filterData[0];
        const filterId = filterData[1];
        return "&" + values.FilterQueryKeys[filterType] + "=" + filterId;
      })
      .join("");
  };

  const requestUrl = urlParams.filters
    ? `${baseUrl}${path}?_page=${urlParams.pageNumber}${getFiltersString(
        urlParams
      )}${urlParams.productIds ? urlParams.productIds : ""}`
    : `${baseUrl}${path}?_page=${urlParams.pageNumber}${
        urlParams.productIds ? urlParams.productIds : ""
      }`;

  useEffect(() => {
    async function init() {
      try {
        let response;
        if (urlParams.pageNumber) {
          response = await fetch(requestUrl);
        } else {
          response = await fetch(baseUrl + path);
        }

        if (response.ok) {
          const json = await response.json();
          setData(json);
        } else {
          throw response;
        }
      } catch (e) {
        setError(e);
      } finally {
        setLoading(false);
      }
    }
    init();
  }, [requestUrl]);

  return { data, error, loading };
}
