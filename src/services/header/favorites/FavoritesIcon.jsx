import React from "react";
import { values } from "../../../constants";
import Icon from "../../../components/global/icon/Icon";

function FavoritesIcon() {
  return (
    <span className={values.FavoritesIconSpan.className}>
      <Icon
        className={values.FavoritesIcon.className}
        iconBoldness={values.FavoritesIcon.style}
        iconName={values.FavoritesIcon.iconName}
      />
    </span>
  );
}

export default FavoritesIcon;
