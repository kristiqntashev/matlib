export default function makeFilterList(e, currentFilters) {
  if (!e.target.name) return [];

  const selectedFilterIndex = currentFilters.indexOf(e.target.name);
  if (selectedFilterIndex !== -1)
    return [
      ...currentFilters.slice(0, selectedFilterIndex),
      ...currentFilters.slice(selectedFilterIndex + 1)
    ];

  return [...currentFilters, e.target.name];
}
