import { connect } from "react-redux";
import { editUserDetails } from "../../services/useFetchProducts";
import { logOut } from "../../actions/authActions";
import UserDetails from "../../components/login/UserDetails/UserDetails";

function mapStateToProps(state) {
  const { details, user } = state;
  return { details, user };
}

const mapDispatchToProps = (dispatch) => {
  return {
    editUserDetail: (details) => dispatch(editUserDetails(details)),
    logOut: (user) => dispatch(logOut(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
