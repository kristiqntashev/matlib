import { connect } from "react-redux";
import { Header } from "../../components/header/Header";

const mapStateToProps = (state) => {
  const { user, favList } = state;
  return { user, favList };
};

export default connect(mapStateToProps)(Header);
