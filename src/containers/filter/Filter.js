import { connect } from "react-redux";
import { filter } from "../../actions/filterActions";
import GridContainer from "../../containers/grid/GridContainer";

function mapStateToProps(state, ownProps) {
  const { currentFilters, sorterType, searching } = state;
  const { productIds } = ownProps;
  return { currentFilters, productIds, sorterType, searching };
}
const mapDispatchToProps = (dispatch) => {
  return {
    setFilter: (currentFilters) => dispatch(filter(currentFilters))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GridContainer);
