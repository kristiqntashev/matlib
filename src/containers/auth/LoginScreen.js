import { connect } from "react-redux";
import { logIn } from "../../actions/authActions";
import LoginScreen from "../../components/login/LoginScreen/LoginScreen";
import { fetchFavorites } from "../../services/useFetchProducts";

function mapStateToProps(state) {
  const { user } = state;
  return { user };
}

const mapDispatchToProps = (dispatch) => {
  return {
    logIn: (user) => dispatch(logIn(user)),
    fetchFavorites: () => dispatch(fetchFavorites(dispatch))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
