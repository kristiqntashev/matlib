export const values = {
  LikeButton: {
    iconName: "heart",
    className: "icon",
    solidStyle: "fas",
    regularStyle: "far"
  },
  TooltipOnProductLike: {
    className: "tooltiptextonproductlike",
    addMe: "Add me to your favorites",
    removeMe: "Remove me from your favorites"
  },
  LikeButtonWithTooltip: {
    className: "tooltiponproductlike"
  },
  FavoritesIcon: {
    iconName: "heart",
    className: "far fa-heart",
    style: "far"
  },
  FavoritesIconSpan: {
    className: "icon-favorites"
  },
  Links: {
    favoritesPage: {
      link: "favorites",
      className: "favorites-link"
    }
  },
  FilterQueryKeys: {
    materials: "materialTypeId",
    colors: "colors",
    tags: "tags"
  },
  FilterTypes: ["materials", "colors", "tags"]
};
