describe("Test scroll functionality", () => {
  it("visit https", () => {
    cy.visit("http://localhost:3000/");
    cy.wait(1000);
    cy.get("#products").find('.product').its('length').should('eq', 20)
    cy.scrollTo('bottom');
    cy.wait(1000);
    cy.get("#products").find(".product").its("length").should("eq", 40);
  });
});
